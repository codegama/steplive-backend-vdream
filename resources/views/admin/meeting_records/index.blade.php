@extends('layouts.admin')

@section('content-header', tr('meeting_management'))

@section('bread-crumb')

<li class="breadcrumb-item" aria-current="page">
    <a href="{{route('admin.meetings.index')}}">{{ tr('meetings') }}</a>
</li>



<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('meeting_records') }}</span>
</li>

@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('meeting_records')}}</h5>

                </div>

                @include('admin.meeting_records._search')

                <table class="table dt-responsive nowrap">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('meeting')}}</th>
                            <th>{{tr('user')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($meeting_records as $i => $meeting_record)

                        <tr>
                            <td>{{$i+$meeting_records->firstItem()}}</td>

                            <td>
                                <a href="{{route('admin.meeting_records.view',['meeting_record_id' => $meeting_record->id])}}"> {{ $meeting_record->unique_id }}
                                </a>
                            </td>

                            <td>
                                <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_record->id])}}">
                                    {{ $meeting_record->meeting->title ?? ''}}
                                </a>
                            </td>

                            <td>
                                <a href="{{route('admin.users.view',['user_id' => $meeting_record->user_id])}}"> {{ $meeting_record->userDetails->name ?? "-" }}
                                </a>
                            </td>

                            <td>

                                <span class="badge badge-secondary"> {{$meeting_record->meeting->status_formatted ?? ''}}</span>

                            </td>

                            <td>

                                <div class="template-demo">

                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{tr('action')}}
                                        </button>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">

                                            <a class="dropdown-item" href="{{ route('admin.meeting_records.view', ['meeting_record_id' => $meeting_record->id]) }}">
                                                {{tr('view')}}
                                            </a>

                                            <a class="dropdown-item" onclick="return confirm(&quot;{{tr('meeting_records_delete_confirmation', $meeting_record->meeting->title ?? '')}}&quot;);" href="{{route('admin.meeting_records.delete', ['meeting_record_id' => $meeting_record->id])}}">
                                                {{tr('delete')}}
                                            </a>

                                        </div>

                                    </div>

                                </div>

                            </td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="float-right">{{ $meeting_records->appends(request()->input())->links() }}</div>

            </div>

        </div>

    </div>

</div>


@endsection