@extends('layouts.admin') 

@section('content-header',tr('support_contacts'))

@section('title', tr('support_contacts'))

@section('bread-crumb')

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('support_contacts') }}</span>
    </li>
           
@endsection 

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
        
    <div class="card">

        <div class="card-body">

            <div class="border-bottom pb-3">

                <h5 class="text-uppercase">{{tr('support_contacts')}}</h5>

            </div>

            <div class="table-responsive pb-3 pt-3">
                 @include('admin.support_contacts._search')
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>
                            <th>{{tr('country')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>


                        @foreach($support_contacts as $i => $support_contact)

                            <tr>
                                <td>{{$i+$support_contacts->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.support_contacts.view' , ['support_contact_id' => $support_contact->id] )}}"> {{$support_contact->name}}</a>
                                </td>

                                <td>{{$support_contact->email}}</td>
                                
                                <td>{{$support_contact->mobile}}</td>

                                <td>{{$support_contact->country}}</td>


                                <td>
                                    <a class="btn btn-success" href="{{route('admin.support_contacts.view', ['support_contact_id' =>  $support_contact->id] )}}">
                                        {{tr('view')}}
                                    </a>
                                </td>
                            
                            </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="float-right">{{$support_contacts->links()}}</div>

            </div>

        </div>
    
    </div>

</div>

@endsection