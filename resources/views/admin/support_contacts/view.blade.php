@extends('layouts.admin') 

@section('content-header',tr('support_contacts'))

@section('title', tr('view_support_contact'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{route('admin.support_contacts.index')}}">{{tr('support_contacts')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">
        <span>{{tr('view_support_contact')}}</span>
    </li>
           
@endsection  

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
        
    <div class="card">

        <div class="card-body">

            <div class="border-bottom pb-3">

                <h5 class="text-uppercase">{{tr('view_support_contacts')}}
              
                </h5>

            </div>

            <div class="card-group">

                <!-- Card -->
                <div class="card mb-4">

                    <!-- Card content -->
                    <div class="card-body">

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('name')}}</h5>
                            
                            <p class="card-text">{{$support_contact->name}}</p>

                        </div> 

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('email')}}</h5>
                            
                            <p class="card-text">{{$support_contact->email}}</p>

                        </div>

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('mobile')}}</h5>
                            
                            <p class="card-text">{{$support_contact->mobile}}</p>

                        </div>

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('country')}}</h5>
                            
                            <p class="card-text">{{$support_contact->country}}</p>

                        </div> 
                                                
                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('updated_at')}}</h5>
                            
                            <p class="card-text">{{common_date($support_contact->updated_at,Auth::guard('admin')->user()->timezone)}}</p>

                        </div>

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('created_at')}}</h5>
                            
                            <p class="card-text">{{common_date($support_contact->created_at,Auth::guard('admin')->user()->timezone)}}</p>

                        </div>

                        <div class="custom-card">
                        
                            <h5 class="card-title">{{tr('message')}}</h5>
                            
                            <p class="card-text">{{$support_contact->message}}</p>

                        </div>

                    </div>
                    <!-- Card content -->

                </div>

                <!-- Card -->

            </div>


        </div>
    
    </div>

</div>


@endsection